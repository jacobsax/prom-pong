#!/usr/bin/python

import RPi.GPIO as GPIO

LED = [5, 6, 12, 13, 16, 19, 20, 26]


class LEDBar:
	
	def __init__(self):
		
		GPIO.setmode(GPIO.BCM)		
		
		for pin in LED:
			GPIO.setup(pin, GPIO.OUT)
		
	def setLed(self, pin):
		
		for p in LED:
			GPIO.output(p, False)
			
		GPIO.output(LED[pin], True)

