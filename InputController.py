#Used to control a single players input

import AnalogueInput
import DigitalInputGPIO

from Plus import *
import time

voltage_reference = AnalogueInput.AnalogueInput(AnalogueInput.PIN_3)

class InputController:

	
	
	class playerInput:
		
		oldPaddlePosition = 8
		paddlePosition = 8
		timer = 0
		paddleMove = False
		buttonAPressed = False
		buttonBPressed = False
		
		def __init__(self, pot_pin, a_pin, b_pin):
			
			self.pot = AnalogueInput.AnalogueInput(pot_pin)
			self.buttonA = DigitalInputGPIO.DigitalInput(a_pin)
			self.buttonB = DigitalInputGPIO.DigitalInput(b_pin)

		def update(self):
			
			pot_value = self.pot.getValue()
			self.oldPaddlePosition = self.paddlePosition
			paddleRange = int(mapToRange(voltage_reference.getValue(), 3000, pot_value))
			if (paddleRange < 500):
				paddleRange = 500
			elif (paddleRange > 2500):
				paddleRange = 2500

			paddleRange = paddleRange - 500

			self.paddlePosition = int(mapToRange(2000, 40, paddleRange))
			#Simple signal filter to filter out errant analogue signals
			if ((self.oldPaddlePosition != self.paddlePosition) and not self.paddleMove):
				self.paddleMove = True
				self.timer = int(time.time() * 1000)
				self.paddlePosition = self.oldPaddlePosition

			if ((self.oldPaddlePosition != self.paddlePosition) and self.paddleMove):
				if ((self.timer + 10) < (int(time.time() * 1000))):
					self.paddleMove = False
				else:
					self.paddlePosition = self.oldPaddlePosition
				
				
			self.buttonAPressed = self.buttonA.getState()
			self.buttonBPressed = self.buttonB.getState()

			if (self.paddlePosition >= 38):
				self.paddlePosition = 36
		
	
	def __init__(self):
		self.position = 8
		self.playerA = self.playerInput(AnalogueInput.PIN_2, DigitalInputGPIO.PLAYER_A_TOP, DigitalInputGPIO.PLAYER_A_BOTTOM)
		self.playerB = self.playerInput(AnalogueInput.PIN_4, DigitalInputGPIO.PLAYER_B_TOP, DigitalInputGPIO.PLAYER_B_BOTTOM)
		
		
	def update(self):
		self.playerA.update()
		self.playerB.update()
		
	def getInput(self):
		
		return self.playerA, self.playerB
