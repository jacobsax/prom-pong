#Helpful additional functions


def mapToRange(originalMax, newMax, originalValue):
	
	factor = float(newMax) / float(originalMax)
	
	return float(originalValue) * float(factor)
	
class Vector2D:
	def __init__(self, x, y):
		
		self.x = x
		self.y = y
		
	def __add__(self, Vector2):
		
		return Vector2D(self.x + Vector2.x, self.y + Vector2.y)
		
	def __mul__(self, constant):
		
		return Vector2D(self.x * constant, self.y * constant)
		
def limit(value, limit):
	if (value < limit):
		return value
	else:
		return limit
