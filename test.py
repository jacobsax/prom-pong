import time


from PyGlow import PyGlow


pyglow = PyGlow()
pyglow.color("white", 100)
time.sleep(0.1)
pyglow.color("blue", 100)
time.sleep(0.1)
pyglow.color("green", 100)
time.sleep(0.1)
pyglow.color("yellow", 100)
time.sleep(0.1)
pyglow.color("orange", 100)
time.sleep(0.1)
pyglow.color("red", 100)
time.sleep(0.5)
pyglow.all(0)
